const functions = require("firebase-functions");
const puppeteer = require("puppeteer");
const cors = require("cors")({ origin: true });

const runtimeOpts = {
  timeoutSeconds: 300,
  memory: "1GB",
};

exports.news = functions
  .runWith(runtimeOpts)
  .https.onRequest(async (req, res) => {
    const browser = await puppeteer.launch({
      args: ["--no-sandbox"],
    });
    const page = await browser.newPage();

    await page.goto("https://www.intelliware.com/news-updates-and-insights/");
    let urls = await page.evaluate(() => {
      let results = [];
      let items = document.querySelectorAll(".h5");
      items.forEach((item) => {
        results.push({
          url: item.getAttribute("href"),
          text: item.innerText,
        });
      });
      return results;
    });

    browser.close();
    return cors(req, res, () => {
      // [START sendResponse]
      res.status(200).send(urls);
      // [END sendResponse]
    });
  });

exports.services = functions
  .runWith(runtimeOpts)
  .https.onRequest(async (req, res) => {
    const browser = await puppeteer.launch({
      args: ["--no-sandbox"],
    });
    const page = await browser.newPage();

    await page.goto("https://www.intelliware.com/");
    let urls = await page.evaluate(() => {
      let results = [];
      let items = document.querySelectorAll(".feature.feature-5");
      items.forEach((item) => {
        results.push({
          iconDescription: item
            .querySelector("span")
            .getAttribute("class")
            .replace(/icon|--lg|-/g, "")
            .trim(),
          title: item.querySelector("h4").innerText,
          text: item.querySelector("p").innerText,
          url: item.querySelector("h5 a").getAttribute("href"),
        });
      });
      return results;
    });

    browser.close();
    return cors(req, res, () => {
      // [START sendResponse]
      res.status(200).send(urls);
      // [END sendResponse]
    });
  });

exports.successStories = functions
  .runWith(runtimeOpts)
  .https.onRequest(async (req, res) => {
    const browser = await puppeteer.launch({
      args: ["--no-sandbox"],
    });
    const page = await browser.newPage();

    await page.goto("https://www.intelliware.com/");
    let urls = await page.evaluate(() => {
      let results = [];
      let items = document.querySelectorAll(".success-stories article");
      items.forEach((item) => {
        results.push({
          img: item.querySelector("a img").getAttribute("src"),
          text: item.querySelector("div.feature__body a.h5").innerText,
          url: item
            .querySelector("div.feature__body a.h5")
            .getAttribute("href"),
        });
      });
      return results;
    });

    browser.close();
    return cors(req, res, () => {
      // [START sendResponse]
      res.status(200).send(urls);
      // [END sendResponse]
    });
  });

exports.readMore = functions.https.onRequest(async (req, res) => {
  const blogAddress = req.query.url;
  const browser = await puppeteer.launch({
    args: ["--no-sandbox"],
  });
  const page = await browser.newPage();

  await page.goto(blogAddress);
  let result = await page.evaluate(() => {
    return {
      itemHTML: document.querySelector("body").innerHTML,
      itemText: document.querySelector("body").innerText,
    };
  });

  browser.close();
  res.status(200).send(result);
});
